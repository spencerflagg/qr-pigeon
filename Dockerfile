# Use Node.js as the base image
FROM node:16-alpine

    # Set the working directory
WORKDIR /app

# Copy the package.json and package-lock.json files
COPY ./ ./

# Install the dependencies
RUN npm install
RUN npm i -g vite
RUN npm run build --host

# Copy the rest of the source code
#COPY . .

# Build the Vite and Svelte app
#RUN npm run build

# Set the environment variable for production
#ENV NODE_ENV=production

# Expose the default port used by the web app
EXPOSE 5173

# Start the app
CMD [ "npm", "start" ]
